---
theme : "night"
transition: "slide"
highlightTheme: "monokai"
logoImg: "assets/logo.png"
slideNumber: true
title: "Apache Flink - Stateful streaming done right"

---

# Ciao! 

---

## Migrating Oracle Data Warehouse to Datalake

AgileLab Internal Conference 

12 January 2024 @ Turin

----

#### Speaker

Andrea Fonti

<img width="30%" src="./assets/mecropped.png">

Big Data Architect @ AgileLab

---

# Agenda

----

* Context
* Solution
* How the customer reacted
* Syntethic dataset generation
* Query execution benchmark
* Is the customer happy now? 



---

# Context 

----

## Usecase

Market vigilance

----


### Ingestion Frontend 
 
```mermaid
graph LR 


  subgraph ORACLEDB
    STAGING_TABLES
    end


  MARKET_DATA --> KAFKA
  KAFKA --> STAGING_TABLES 
```

**Low Latency!**

----

### Backend 

```mermaid
graph LR 


  subgraph ORACLEDB


    STAGING_TABLES --> TRANSFORMATIONS 
    TRANSFORMATIONS --> TARGET_TABLES

    end

```

Traditional cozy datawarehouse

----


#oraclecollectingmoney

<img style="text-align:left" src="./assets/ora.png" width="100%"/>

----

### The need

Reducing costs, oracle contract is 2M upfront plus usage charges

Customer is constantly monitoring usage and applying manual scaling to achieve the desired costs

Have you heard of `MIPS` reduction?

Here we go again

----


### Transformations 

```mermaid
graph TD 
  PL/SQL
  SCHEDULER

  SCHEDULER --> PL/SQL
```

Feels like ancient tech 

(and tastes like it too)

----

![wizard](./assets/wizard.jpeg)



----

### The need

enable usage of modern data centric technologies

search for `python`, `java` compentencies instead of rare `PL/SLQ` developers


----



### Query Frontend

```mermaid
graph LR 


  subgraph ORACLEDB


    
    TARGET_TABLES --> VIEWS

    VIEWS -->VIEWS

    end

    CLIENTS --> VIEWS

```

----

Clients are mostly `odbc` based in a `.net` environment

They should change as little as possible

----

## Numbers

| | |
|---------------|-------|
| Nodes         | 2     |
| Cores         | 100   |
| Ram           | 2.7 TB |

----

## Numbers

| | |
|---------------|-------|
| Messages/s         | 90 K   |
| Messages/daily| 1.7 G |
| Staging DB size| .8 TB <br> *35 GB*|
| Live DB Size | 60 TB <br> *25 TB*|
| Historical DB Size | 20 TB <br> *11 TB*|

---

# Solution

----

## A Datalake

* Open standards
* Cloud ready
* No vendor lockin
* Decoupled compute and storage

----

![layers](./assets/layers.png)


----

## Components

| | |
|--------------|--------|
| Dremio | Query Engine      |
| Spark | Batch ETL|
| Flink | Streaming ETL|
| Minio | S3 Compatible storage|
| Kubernetes | Resource|



----

## What can you buy with the same money?
----

## Computational Cluster

| | |
|--------------|--------|
| Nodes        | 16     |
| Cores/Node   | 56     |
| Memory/Node  | 512 GB |
| NVME Storage | 86TB   |
| Network      | 100Gb Ethernet  |

----

## Storage Cluster

| | |
|--------------|--------|
| Nodes        | 4      |
| Cores/Node   | 384    |
| Memory/Node  | 4 TB |
| NVME Storage | 384 TB|
| Available minio storage| 1 PB|
| Network      | 100Gb Ethernet  |


---

## The 1 million dollars question

----


## Will the new architecture be compliant with service level agreements? 


----

## Lets be data driven!

----

## What we have

* SQL DDL files of the data warehouse schema
* Query SQL 
* Query usage data
* Oracle computed table statistics
 
---

## Question 1

The oracle data warehouse is able to heavily compress data thanks to Hybrid Columnar Compression

Will a datalake retain the same compression ratio using iceberg format?


----

## Can we export the original dataset to iceberg?

Of course not, the original systems serves an highly regulated environment 

----

## Understand table schemas

```scala
def loadCatalog(): 
  Map[FullyQualifiedTableName, Map[String, Column]]

case class Column(dtype: String, nullable: Boolean)
```



----

## Built with JSQL Parser library 

```scala
"com.github.jsqlparser" % "jsqlparser" % "4.8"
```

* JSQL Parser offers a typesafe api to parse sql statements
* No string manipulations

----

## Lets load table statistics

```scala
def loadStats(
  catalog: Map[FullyQualifiedTableName, Map[String, Column]]
): Map[FullyQualifiedTableName, TableStatistics]

```

----

```scala
case class TableStatistics(
    numberOfRows: Long,
    averageSpace: Double,
    averageRowLength: Double,
    scanRate: Double,
    sampleSize: Long,
    columns: Map[String, ColumnStatistics]
)
```

----


```scala
case class ColumnStatistics(
    numberOfDistinctValues: Long,
    lowValue: String,
    highValue: String,
    density: Double,
    numberOfNulls: Long,
    averageColumnLength: Double,
    histogram: Option[DistributionHistogram],
    rawLowValue: Array[Byte],
    rawHighValue: Array[Byte],
    oracleParsedLowValue: Option[String],
    oracleParsedHighValue: Option[String]
)
```

----

## Frequency histogram

In a frequency histogram, each distinct column value corresponds to a single bucket of the histogram. Because each value has its own dedicated bucket, some buckets may have many values, whereas others have few.

----

<img src="./assets/frequency_histograms.png" width="40%">
----

```scala
case class FrequencyHistogram(info: List[ElementFrequency])
case class ElementFrequency(
    element: String,
    frequency: Long,
    rawElement: Array[Byte],
    charValue: String
)
```
----

## Height balanced hystograms

In a height-balanced histogram, column values are divided into buckets so that each bucket contains approximately the same number of rows.

----

For example, if you have 99 coins to distribute among 4 buckets, each bucket contains about 25 coins. The histogram shows where the endpoints fall in the range of values.

----

<img src="./assets/height_balanced_histograms.png" width="40%">

----

```scala
case class HeightBalancedHistogram(
    info: List[HeightBalancedElement]
)
case class HeightBalancedElement(
    start: String,
    stop: String,
    weight: Long,
    rawStart: Array[Byte],
    rawStop: Array[Byte],
    charStart: String,
    charStop: String)
```
----

## Hybrid histograms

A hybrid histogram combines characteristics of both height-based histograms and frequency histograms. 

----

Height-balanced histograms do not store as much information as hybrid histograms. By using repeat counts, the optimizer can determine exactly how many occurrences of an endpoint value exist. 

----

<img src="./assets/hybrid_histograms.png" width="40%">

----

```scala
case class HybridHistogram(info: List[HybridElement])
case class HybridElement(
    start: String,
    stop: String,
    weight: Long,
    repeatCount: Long,
    rawStart: Array[Byte],
    rawStop: Array[Byte],
    charStart: String,
    charStop: String
```
----

## Lets build a data generator

----

```mermaid
graph LR 
  Catalog --> Generator
  Statistics --> Generator
  RandomSeed --> Generator
  Generator --> Partition

```

```scala
  def generateRow(
      stats: TableStatistics,
      struct: StructType,
      random: Random
  ): Row 
```

----

```scala
spark
  .range(0,  tableStatistics.numberOfRows, 1, parallelism)
  .mapPartitions { partition: Iterator[lang.Long] =>
    val random = new java.util.Random(
      TaskContext.getPartitionId() + seed)

    partition.map(x =>
      Generator.generateRow(tableStatistics, schema, random)
    )
  }
```
----

Map SQL Types to Spark Types

```scala
  case numWithPrecisionAndScalePattern(precision, scale) =>
    DecimalType(
      precision = precision.toInt,
      scale = scale.toInt
    )
  case numWithPrecisionPattern(precision) 
    if (precision.toInt <= 18) => LongType
  case numWithPrecisionPattern(precision) 
    if (precision.toInt > 18) =>
      DecimalType(precision = precision.toInt, scale = 0)
  case bareNumber() => DecimalType(precision = 38, scale = 0)
  case timestampPattern() => TimestampType
  case varcharPattern()   => StringType
  case charPattern()      => StringType
  case datePattern()      => DateType
```

----

## Frequency histogram

* Determine null ratio of table, choose null or continue
* use weight of bucket as probability to select a bucket
* use the complete element from the bucket

----

## Hybrid histograms

* Determine null ratio of table, choose null or continue
* Use weight of bucket as probability to select a bucket
* Assume normal distribution inside the bucket
* Generate elements between bucket start and stop values
----

## Height balanced histograms

* Determine null ratio of table, choose null or continue
* Use weight of bucket as probability to select a bucket
* Assume normal distribution inside the bucket
* Generate elements between bucket start and stop values
----

## No histograms

* Determine null ratio of table, choose null or continue
* Generate elements between table level start and stop values

----

## Generate datasets

* Automatically choose tables to generate datasets for by inspecting catalog and stats
* Use programmatic criterias (wide table, narrow table, big table, small table)

----

## Measure disk space usage

* Collect generated file size

----

## Tweak the search space

* compression type: none, snappy, gzip
* mapped datatypes

----

## Interesting discoveries

DecimalType precision and scale matters for parquet

----

* if decimal can be safely stored in an integer use integer
* if decimal can be safely stored in an long use long 
* use byte[] for other cases (lots of columnar optimizations disable here, like RLE)

----

## Answer 1

* Generated datasets immediately look familiar to the Subject Matter Experts
* No laws where broken by exporting data
* Outcome of the benchmark looks good
  
----

## Customer is happy and confident


---

# Question 2

Will query performance be comparable (or better) with the current system?

----

* Develop an infrastructure as code deployment of the target stack
* Use query plan execution data to select a meaningful query (yay lots of joins) 
* Generate al the needed tables with the data generator
* Run a benchmark scenario of the infrastructure
* Rinse and repeat changing infrastructure parameters

----

| Table | Scenario SMALL | Scenario Large   |
|-|----------------|------------------|
| T1 | 13| 13|
| T2 | 100.000.000,00 | 818.061.276,00   |
| T3 | 100.000.000,00 | 1.000.000.000,00 |
| T4 | 100.000.000,00 | 1.000.000.000,00 |
| T5 | 100.000.000,00 | 1.000.000.000,00 |
| T6 | 100.000.000,00 | 1.000.000.000,00 |
| T7 | 5.036.091,00   | 5.036.091,00     |
| T8 | 26.559.749,00  | 26.559.749,00    |
| T9 | 5.800.462,00   | 5.800.462,00     |
| T10 | 3.488.659,00   | 3.488.659,00     |


----

![plan](./assets/plan.png)

----

![minio](./assets/minio-space.png)


----

* Original query accesses last ten minutes of data
* Lets have as goal accessing the last daily partition and measure query on the whole dataset as well

----

### Full dataset access large

![large-access-full](./assets/large-access-full.png)


----

### Daily partition dataset access large

![large-access-daily](./assets/large-access-daily.png)

----


### Daily partition dataset access small

![small-access-full](./assets/small-access-full.png)


----

### Daily partition dataset access small

![small-access-daily](./assets/small-access-daily.png)


----

## Customer is happy and confident

Query performances are on par with the current system

----

## Be data driven

* Use current usecase as a datasource
* Analyze metadata as you would analyze data
* Develop analysis as code (reproducible, parameterized)
* Develop infrastructure as code ( reproducible, parameterized)

---


# #Thanks

publicagilefactory.gitlab.io/
presentations/oracle-to-datalake-migration/main/#/

----

# Questions?